from tabula import convert_into
import argparse


def convert_files(infile, outfile, output_format='csv'):
    convert_into(infile,
                 outfile,
                 output_format=output_format,
                 pages='all')


def craft_args():
    '''
    Craft the arguments with ArgParse

    :returns: ArgParse object with arguments
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--infile', type=str, required=True,
                        help='File to convert')
    parser.add_argument('-t', '--type', type=str,
                        help='Type of outfile (csv/tsv)')
    parser.add_argument('-o', '--outfile', type=str, required=True,
                        help='File to output to')
    return parser.parse_args()


if __name__ == '__main__':
    args = craft_args()
    if not args.type:
        file_type = 'csv'
    else:
        file_type = args.type
    convert_files(args.infile, args.outfile, file_type)
