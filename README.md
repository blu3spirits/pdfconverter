# Quick PDF converter

Note: Will not work with really weird PDFS

### Quickstart
```
git clone https://gitlab.com/blu3spirits/pdfconverter.git
cd pdfconverter/
python -m venv .
source bin/activate
pip install -r requirements.txt
python convertme.py -f INPUT_FILE -o OUTPUT_FILE [-t PDF_TYPE]
```
